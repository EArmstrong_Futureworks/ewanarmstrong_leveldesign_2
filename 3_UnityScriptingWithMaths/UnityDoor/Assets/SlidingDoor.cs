﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float moveAmount = 1f;
    public float snap = 0.02f;
    public float speed = 5f;
    public AudioSource audioSource;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
    //opens door
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", moveAmount);
        }
    }

    //closes door
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    //Moves the door to target
    IEnumerator DoorMove(float target)
    {
        audioSource.Stop();
        audioSource.Play();
        float xPos = door1Tran.localPosition.x;
        while (xPos < (target - snap) || xPos > (target + snap))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        yield return null;
    }
}
