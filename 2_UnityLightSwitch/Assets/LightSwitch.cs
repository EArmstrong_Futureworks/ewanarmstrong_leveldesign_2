﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener trigger;
    public Image cursorImage;

    //light variable
    public Light spotLight;
    //audio variable
    public AudioSource switchAudio;
    //animation variable
    Animation anim;


	// Use this for initialization
	void Start ()
    {
        switchAudio = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        cursorImage.enabled = false;
    }
	
	// called when cursor is over object
	void OnMouseOver ()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
            Debug.Log("Mouse over switch");
        }
        else
        {
            cursorImage.enabled = false;
        }
	}

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            switchAudio.Play();
            anim.Stop();
            anim.Play();

            if (spotLight.intensity > 0f)
            {
                spotLight.intensity = 0f;
            }
            else
            {
                spotLight.intensity = 1.5f;
            }
        }
    }
}