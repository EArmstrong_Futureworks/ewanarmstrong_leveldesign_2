﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//These are namespaces: locations of code that we need.

//This is a class that inherits from MonoBehaviour
public class TriggerListener : MonoBehaviour
{
    //At the top of classes we decare our variables
    public bool playerEntered = false;

	// Use this for initialization
    void Start ()
    {
        Debug.Log("Start was called");
    }
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was called");
    }

    //Checks if player enters trigger
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    //Checks if player exits trigger
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
}